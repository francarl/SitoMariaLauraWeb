package org.francescocarlucci.sito.marialaura;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CrawlFilter implements Filter {

	private static final String G_CRAWL_KEYWORD = "_escaped_fragment_";
	//private static Logger logger = Logger.getLogger(CrawlFilter.class);
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		String queryString = req.getQueryString();
		String requestURI = req.getRequestURI() + "?" + queryString;
		
		System.out.println("DOFILTER on url = " + requestURI );
		
		if (queryString != null && queryString.contains(G_CRAWL_KEYWORD)) {
			
				
			String path = req.getParameter(G_CRAWL_KEYWORD);
			path = path.replaceAll("/$", "");
					
            String newURI = "/snapshots" + path.concat(".html");
            req.getRequestDispatcher(newURI).forward(req, res);
				
				
//				String html = null;
//				
//				// send result back
//				res.setContentType( "text/html;charset=UTF-8" );
//			    PrintWriter out = res.getWriter();
//			    
//			    out.println( html );
//			    out.close();
			
			
		} else {
			chain.doFilter(request, response);
		}
			
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	
	public String rewriteQueryString(String url_with_escaped_fragment) {
	    try {
	      String decoded = URLDecoder.decode(url_with_escaped_fragment, "UTF-8");

	      // this helps run on development mode
	      String gwt = decoded.replace("gwt", "?gwt");
	      String unescapedAmp = gwt.replace("&_escaped_fragment_=", "#!");
	      String result = unescapedAmp.replace("_escaped_fragment_=", "#!");
	      return result;
	    } catch (UnsupportedEncodingException e) {
	      // logger_.log(Level.SEVERE, e.toString());
	      return "";
	    }
	  }
}
