'use strict';

/* Controllers */

app.controller('SiteController', function($rootScope, $scope, $state, $modal, $location, $anchorScroll) {

  $scope.ready = false;

  $scope.$on('$viewContentLoaded', function(event) {
    $scope.ready = true;
  });

  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) { 
    $scope.ready = false;
  });


  $rootScope.$on('$stateChangeSuccess', function() { 
    $location.hash('top');
    $anchorScroll();
  });



  $scope.openPrivacyPolicy = function() {

    var modalInstance = $modal.open({
     templateUrl: 'partials/privacy.html',
     windowClass: 'privacy-modal'
    });
    
  }


  $scope.animateInView = function(inview, element, cssAnimation) {
    if (inview) {
      angular.element(element).children().removeClass("hidden").addClass(cssAnimation);
    }
  }

});

app.controller('NavBarController', function($rootScope, $scope, $state) {

  $scope.isCollapsed = true;

  $rootScope.$on('$stateChangeSuccess', function() {
	  
      $scope.isCollapsed = true;        
    });

  
  $scope.goTo = function (newState, newParams) {
	  
	  $state.go(newState, newParams);
	  
  }

  // gestione menu a tendina

  
});




app.controller('BlogController', function($scope,  $http, $log, $stateParams, $sce) {

	$scope.refreshFeeds = function () {

		var feedUrl = "https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://francescoagnese.blogspot.com/feeds/posts/default&callback=JSON_CALLBACK";

		$http.jsonp(feedUrl).
			success(function(data, status, headers, config) {
				
				$scope.feeds = data.responseData.feed.entries;
				
				if ($stateParams.post) {
					$scope.feeds[$stateParams.post].active = true;
				}
			});
	};
	
	$scope.showFeed = function (index) {
        $scope.activeFeed = $scope.feeds[index];
        
        $scope.activeFeed.content = $sce.trustAsHtml($scope.activeFeed.content);
        
        $scope.disqus_id = $scope.feeds[index].title.replace(/\s+/g, '');
	}

	$scope.refreshFeeds();
	
//	$scope.$watch('$stateParams.post', function (newVal) {
//		
//		console.log("watching " + newVal);
//		
//		$scope.showFeed(newVal);
//	});
});



/*
app.controller('SlideShowController', function($scope, $timeout, $state) {
    
    $scope.ready = true;

    $scope.next = function () {
        var newIndex =  $scope.slides.indexOf($scope.activeSlide) + 1;

        if (newIndex  >= $scope.slides.length) {
          newIndex = 0;
        }

        $scope.activeSlide = $scope.slides[newIndex];
          
    }
    
    
    function rotateSlide () {
        
        var newIndex =  $scope.slides.indexOf($scope.activeSlide) + 1;

        if (newIndex  >= $scope.slides.length) {
          newIndex = 0;
        }

        $scope.activeSlide = $scope.slides[newIndex];
        
        $timeout(rotateSlide, 3000);


    }
    
    
    $scope.$watch(function () {
          return $state.current.data && $state.current.data.slides;
        }, function (newValue) { 
                    
          if (typeof newValue != 'undefined') {

            $scope.slides = newValue;
            $scope.activeSlide = $scope.slides[0];
            
            if (!$scope.animationStarted) {
              $timeout(rotateSlide, 3000);
              $scope.animationStarted = true;
            }
          }
        });

    $scope.animationStarted = false;

});
*/


app.controller('ContattiController', function($scope, $http) {

  $scope.sendRequest = function() {
  
    
    $scope.alerts.length = 0;
    
    if (!$scope.richiesta.email && !$scope.richiesta.cellulare) {

      $scope.alerts.push( {type: "danger", msg: "Inserisci email o cellulare"} );
      return;
    } 
    
    if ( !$scope.richiesta.autorizzo ) {
    	$scope.alerts.push( {type: "danger", msg: "Devi autorizzare il trattamento dei dati personali"} );
        return;	
    }
    
	var mailUrl = "https://script.google.com/macros/s/AKfycbzXPAiXC5kf5oYOKSbdFjAVnd08VHYVrn7QYI_nLeypg3Y_9-4/exec?prefix=JSON_CALLBACK&dati=" + JSON.stringify($scope.richiesta);
	
	$scope.sending = true;
	
	$http.jsonp(mailUrl)
	    .success(function(data, status, headers, config) {
	
	        $scope.alerts.push( {type: "success", msg: "La richiesta e' stata inviata correttamente. Verrai ricontattato al piu' presto. Grazie!" } );
	
	        $scope.sending = false;
	
		}).error(function(data, status, headers, config) {
		    $scope.alerts.push( {type: "danger", msg: "Si e' verificato un errore" } );

		    $scope.sending = false;
		
		});
  }

  $scope.sending = false;

  $scope.alerts = [];

  $scope.richiesta = {};

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };


  $scope.clearForm = function() {
    $scope.alerts.length = 0;
    $scope.richiestaForm.$setPristine();
    $scope.richiesta = {};
  }; 
  
  
  
  // download della contact card
  var content = 'BEGIN:VCARD\r\nVERSION:2.1\r\nN:Tortorella;Maria Laura;;dr.ssa\r\nFN:Maria Laura Tortorella\r\nTITLE:Biologa nutrizionista\r\nTEL;WORK;PREF:+39 3491623792\r\nEMAIL:mltortorella.nutrizione@gmail.com\r\nURL:www.mltortorella-nutrizione.it\r\nEND:VCARD\r\n';
  var blob = new Blob([ content ], { type : 'text/vcard' });
  $scope.vcfUrl = (window.URL || window.webkitURL).createObjectURL( blob );

});

  
  
  
  
  
  
app.controller('DoveSiamoController', function($rootScope, $scope, $http) {  
  
  

  // Mappa google
  $scope.mapOptions = {
      center: new google.maps.LatLng(41.93103, 12.53527),
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  $scope.posizioni = [
    {
      id: 0,
      citta: 'Roma',
      indirizzo: 'Zona Montesacro',
      cap: '00100',
      email: 'mltortorella.nutrizione@gmail.com',
      telefono: '+39 3491623792',
      location: {
        lat: 41.93103, 
        lng: 12.53527
      }
    },
    {
        id: 1,
    	citta: 'Parma',
      indirizzo: 'Centro',
        cap: '00000',
        email: 'mltortorella.nutrizione@gmail.com',
        telefono: '+39 3491623792',
        location: {
          lat: 44.7974536, 
          lng: 10.3238655
        }
      },
      {
          id: 2,
          citta: 'Lecce',
          indirizzo: 'Quartiere S. Lazzaro',
          cap: '00000',
          email: 'mltortorella.nutrizione@gmail.com',
          telefono: '+39 3491623792',
          location: {
            lat: 40.3541308,
            lng: 18.1742944
          }
        }
    
  ]; 

  $scope.radioModel = {
		  id: 0
  	};

  $scope.selectedPosizione = $scope.posizioni[0];
  
  $scope.selezionaCitta = function(index) {
	  $scope.selectedPosizione = $scope.posizioni[index];
	  $scope.center = new google.maps.LatLng($scope.selectedPosizione.location.lat, $scope.selectedPosizione.location.lng);

    $rootScope.$broadcast('cambioCittaEvent', index);
  }

  //infoWindow.open(marker.getMap(), marker);
  
  // init  
  $scope.selectedPosizione = $scope.posizioni[0];
  $scope.center = new google.maps.LatLng($scope.selectedPosizione.location.lat, $scope.selectedPosizione.location.lng);
});



app.controller('LocalCarouselController', function($scope, $rootScope, $state, $sce, $interval) {

  $scope.slides = [];

	$scope.defaultSlides = [
	                    { banner: "img/banner1.png", author: "C. Kousmine", quote: "Il primo e il piu' importante dei doveri che abbiamo e' quello di imparare a preservare dal deterioramento questo prodigio meraviglioso che e' il nostro corpo, a rispettarlo e a nutrirlo bene. Ma per riuscirci e' necessario prima di tutto istruirsi: CONOSCERE PER SOPRAVVIVERE!", link: "link2"},
	                    { banner: "img/banner2.png", author: "Ippocrate", quote: "Fa' che il cibo sia la tua medicina e che la medicina sia il tuo cibo.", link: "link2"},
	                    { banner: "img/banner3.png", author: "Detto popolare", quote: "Colazione da re, pranzo da principe, cena da povero", link: "link2"}
	                   ];
	
	$scope.intervallo = 15000;

  var promise = null;

  $scope.restartTimer = function() {

    $interval.cancel(promise);

    promise = $interval(function(){
        $scope.next();
      }, $scope.intervallo);
  };
 
  $scope.sanitize = function(content) {
    return $sce.trustAsHtml(content);
  }
	
  $scope.select = function(index) {
    $scope.selectedSlideIndex = index;

    $scope.restartTimer();
  }

  $scope.prev = function() {

    $scope.selectedSlideIndex--;
    if ($scope.selectedSlideIndex < 0 ) {
      $scope.selectedSlideIndex = $scope.slides.length - 1;
    }

    $scope.restartTimer();
  }

  $scope.next = function() {

    $scope.selectedSlideIndex++;
    if ($scope.selectedSlideIndex > $scope.slides.length - 1) {
      $scope.selectedSlideIndex = 0;
    }

    $scope.restartTimer();
  }
	
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

		if (toState.name === 'dovesiamo') {

      $scope.slides = [
                      { content: '<div class="row dove-siamo"><div class="col-dove-siamo col-md-3 col-md-offset-1"><div class="caption-dove-siamo">Roma</div></div><div class="col-dove-siamo col-md-4"><div class="foto-dove-siamo foto1-dove-siamo"><img src="img/site/banner_roma_1.jpg" /></div></div><div class="col-dove-siamo col-md-4"><div class="foto-dove-siamo foto2-dove-siamo"><img src="img/site/banner_roma_2.jpg" /></div></div></div>' },
                      { content: '<div class="row dove-siamo"><div class="col-dove-siamo col-md-3 col-md-offset-1"><div class="caption-dove-siamo">Parma</div></div><div class="col-dove-siamo col-md-4"><div class="foto-dove-siamo foto1-dove-siamo"><img src="img/site/banner_parma_1.jpg" /></div></div><div class="col-dove-siamo col-md-4"><div class="foto-dove-siamo foto2-dove-siamo"><img src="img/site/banner_parma_2.jpg" /></div></div></div>' },
                      { content: '<div class="row dove-siamo"><div class="col-dove-siamo col-md-3 col-md-offset-1"><div class="caption-dove-siamo">Lecce</div></div><div class="col-dove-siamo col-md-4"><div class="foto-dove-siamo foto1-dove-siamo"><img src="img/site/banner_lecce_1.jpg" /></div></div><div class="col-dove-siamo col-md-4"><div class="foto-dove-siamo foto2-dove-siamo"><img src="img/site/banner_lecce_2.jpg" /></div></div></div>' }                                            
                  ];
    } else {
      $scope.slides = $scope.defaultSlides;
    }
  
    $scope.selectedSlideIndex = 0;

    $scope.restartTimer();
	});



  $rootScope.$on('cambioCittaEvent', function(event, cittaIdx) {

    $scope.select(cittaIdx);
  });


});


app.controller('ToTopController', function($scope) {
	$scope.tornaSu = function () {
        $("body").animate({scrollTop: 0}, "slow");
    };
});
