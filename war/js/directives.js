'use strict';

/* Directives */


app.directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);

// https://gist.github.com/ProLoser/6136208
app.directive('uiLadda', [function () {
    return {
        link: function postLink(scope, element, attrs) {
            var ladda = Ladda.create(element[0]);
            scope.$watch(attrs.uiLadda, function(newVal, oldVal){

				if (angular.isNumber(oldVal)) {
                    if (angular.isNumber(newVal)) {
                        ladda.setProgress(newVal);
                    } else {
                        newVal && ladda.setProgress(0) || ladda.stop();
                    }
                } else {
                    newVal && ladda.start() || ladda.stop();
                }
            });
        }
    };
}]);

// https://gist.github.com/ProLoser/6136208
app.directive('fcScrollTo', function () {
    return {
        restrict: 'A',
        scope: {
            fcScrollTo: '@',
            fcCurrentPosition: '@'
        },
        link: function (scope, element, attrs) {
            
               attrs.$observe( 'fcCurrentPosition', function(newVal) {

                if (newVal == scope.$eval(attrs.fcScrollTo)) {

                    $("body").animate({scrollTop: $(element).offset().top - 100}, "slow");
                }

                
            });
        }
    };
});

/*
app.directive('fcBackToTop', function () {
    return {
        restrict: 'A',
        scope: {
            fcIcon: '@',
            fcText: '@'
        },
        template: '<a class="back-to-top" ng-click="vai()"><span>{{ fcText }} <i class="fa {{ fcIcon }}"></i></span></a>',
        link: function (scope, element, attrs) {
            
            scope.vai = function () {
                $("body").animate({scrollTop: 0}, "slow");
            };
        }
    };
});


app.directive('fcBackToTop', function ($window) {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            fcText: '@'
        },
        template: '<a class="to-top" ng-click="tornaSu()">{{ fcText }}</a>',
        link: function (scope, element, attrs) {
        	
        	var windowEl = angular.element($window);
            windowEl.on('scroll', function() {
            	
            	if (windowEl.scrollTop()) {
            		element.removeClass("on off");
            		element.addClass("on");
            	} else {
            		element.removeClass("on off");
            		element.addClass("off");
            	}
            });
            
            //element.hide();
            
        	
            scope.tornaSu = function () {
                $("body").animate({scrollTop: 0}, "slow");
            };
        }
    };
});
*/


app.directive('fcScrollDown', function($window) {
    return {
        restrict: 'A',
        scope: {
            fcScrollDown: '='
        },
        link: function(scope, elem, attrs) {
        	
        	var windowEl = angular.element($window);
        	windowEl.on("scroll", function() {
                if (windowEl.scrollTop()) {
                    scope.fcScrollDown = true;  
                } else {
                	scope.fcScrollDown = false;
                }
                
                scope.$apply();
            });
        }
    };
});

app.directive('dnDisplayMode', function($window) {
        return {
            restrict: 'A',
            scope: {
                dnDisplayMode: '='
            },
            template: '<span class="mobile"></span><span class="tablet"></span><span class="tablet-landscape"></span><span class="desktop"></span>',
            link: function(scope, elem, attrs) {
                var markers = elem.find('span');
 
                function isVisible(element) {
                    return element
                        && element.style.display != 'none'
                        && element.offsetWidth
                        && element.offsetHeight;
                }
 
                function update() {
                    angular.forEach(markers, function (element) {
                        if (isVisible(element)) {
                            scope.dnDisplayMode = element.className;
                            return false;
                        }
                    });
                }
 
                var t;
                angular.element($window).bind('resize', function() {
                    clearTimeout(t);
                    t = setTimeout(function() {
                        update();
                        scope.$apply();
                    }, 300);
                });
 
                update();
            }
        };
    });


/*
app.directive('fcDropdown', ['$document', '$location', function ($document, $location) {
  
  var openElement = null;

  return {
    restrict: 'A',
    scope: {
        withHover: '@fcDropdownWithHover',
        onChange: '&fcDropdownOnChange'
    },

    link: function(scope, element, attrs) {

      var closeMenu = function(event) {
        event.preventDefault();
        event.stopPropagation();        

      }

      var openMenu = function(event) {
        event.preventDefault();
        event.stopPropagation();        

        openElement = element;
        
      }

      scope.$watch('$location.path', function() { closeMenu(); });
      
      element.parent().bind('click', function() { 
            closeMenu(); 
        });
      

      element.bind('click', function (event) {

        var elementWasOpen = (element === openElement);

        event.preventDefault();
        event.stopPropagation();

        if (!!openElement) {
          closeMenu();
        }

        if (!elementWasOpen && !element.hasClass('disabled') && !element.prop('disabled')) {
          element.parent().addClass('open');
          openElement = element;
          closeMenu = function (event) {
            if (event) {
              event.preventDefault();
              event.stopPropagation();
            }
            $document.unbind('click', closeMenu);
            element.parent().removeClass('open');
            closeMenu = angular.noop;
            openElement = null;
          };
          $document.bind('click', closeMenu);
        }
      });
    }
  };
}]);
*/
