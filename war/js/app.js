'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('myApp', [
  'ngAnimate',
  'ui.router',
  'ui.bootstrap',
  'angular-carousel',
  'ngDisqus',
  'AngularGM',
  'angular-inview'
]);

app.run(
      ['$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {

        // It's very handy to add references to $state and $stateParams to the $rootScope
        // so that you can access them from any scope within your applications.For example,
        // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
        // to active whenever 'contacts.list' or one of its decendents is active.
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
      }]
);

app.config(function($stateProvider, $urlRouterProvider) {
          //
          // For any unmatched url, redirect to /state1
          $urlRouterProvider.otherwise("/home");
          //
          // Now set up the states
          $stateProvider
            .state('home', {
              url: "/home",
              templateUrl: "partials/home.html",
              data: {
                title: "Benvenuti"
              }
            })
            .state('metodo', {
              url: "/metodo",
              templateUrl: "partials/metodo.html",
              data: {
                title: "Metodo di lavoro"
              }
            })
            .state('contatti', {
              url: "/contatti",
              templateUrl: "partials/contatti.html",
              controller: 'ContattiController',
              data: {
                title: "Contatti"
              }

            })
            .state('dovesiamo', {
              url: "/dovesiamo",
              templateUrl: "partials/dovesiamo.html",
              controller: 'DoveSiamoController',
              data: {
                title: "Dove sono"
              }

            })            
            .state('blog', {
              url: "/blog?post",
              templateUrl: 'partials/blog.html',
              controller: 'BlogController',
              data: {
                title: "Blog"
              }
            })
            .state('servizi', {
              url: "/servizi/:pos",
              templateUrl: 'partials/servizi.html',
              data: {
                title: "Servizi"
              }
            })          
            .state('areaprivata', {
              url: "/login",
              templateUrl: 'partials/login.html',
              data: {
                title: "Login"
              }
            });;
        }
);

app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);

app.config(function($logProvider){
  $logProvider.debugEnabled(true);
});


app.config(function($disqusProvider){
  $disqusProvider.setShortname("sitomarialaura");
});


app.config(['$locationProvider', function($location) {
  $location.hashPrefix('!');
}]);

app.config(function($compileProvider){
	$compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|mailto|tel):/); 
});

app.config(function($uiViewScrollProvider) {
  $uiViewScrollProvider.useAnchorScroll();
});

